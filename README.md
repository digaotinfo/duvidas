# Duvidas React Native + Expo



## Novo Projeto
```
npx create-expo-app -t
```

## Rodar Projeto
```
npx expo start
```

## Modelos para tirar duvidas
### [Skeleton](skeleton.md)
### [Styled Components](styled-components.md)
### [Gerar Build](build.md)
### [Publicar](publicar.md)
### [Ambiente](ambiente.md)
### [SVG](svg.md)
### [Expo Fonts](expo-fonts.md)
### [Expo Icons](expo-icons.md)
### [Expo Router](expo-router.md)
### [ENV](env.md)
### [Zustand](zustand.md)
### [Utils](utils.md)
### [Nativewind](nativewind.md)

