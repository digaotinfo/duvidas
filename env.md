## Instalação
```bash
npm i babel-plugin-inline-dotenv
```
config
```json
plugins: ['inline-dotenv']
```

criar arquivo .env (add no .gitignore criar .env.sample)

## .env
```.environments
NOME_VARIAVEL=ABCD
```

USAR
```typescript
const { NOME_VARIAVEL } = process.env;
console.log(NOME_VARIAVEL);
```