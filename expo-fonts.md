## Instalação
npx expo install expo-font @expo-google-fonts/NOME_DA_FONT
```
npx expo install expo-font @expo-google-fonts/inter
```

no App.tsx
```
import {
    useFonts,
    Inter_400Regular,
    Inter_500Medium,
    Inter_600SemiBold,
    Inter_700Bold
} from '@expo-google-fonts/inter'
import { Loading } from '@/components/loading';

export default function App() {

    const [fonsLoaded] = useFonts({
        Inter_400Regular,
        Inter_500Medium,
        Inter_600SemiBold,
        Inter_700Bold
    });

    if (!fonsLoaded) {
        return <Loading />
    }

...

}
```
