## Instalação

```
npm i nativewind@2.0.11
npm i tailwindcss@3.3.2 --save-dev
```

## Inicializar tailwind 
```
npx tailwindcss init
```


## Config
### tailwind.config.js
```
content: [
    "./src/app/**/*.{ts,tsx}", "./src/components/**/*.{ts,tsx}"
  ],
```

## Fonte perssonalizada
```javascript
theme: {
  extend: {
    fontFamily: {
      heading: "Inter_600SemiBold",
      subtitle: "Inter_500Medium",
      body: "Inter_400Regular",
      bold: "Inter_700Bold", 
    }
  },
}
```

### babel.config.js
```
plugins: ['nativewind/babel']
```

### Types
```typescript
/// <reference types="nativewind/types" />
```


## Usar
```javascript
<SafeAreaView className="flex-1 bg-slate-900">
  <HOME />
</SafeAreaView>
```

## Usar cor padrão em outras partes
```typescript
import colors from "tailwindcss/colors";
import { Feather } from "@expo/vector-icons";

export function Home() {
  <Feather name="shopping-bag" color={colors.white} size={24} />
}
```