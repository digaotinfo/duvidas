## PUBLICAR APP => https://reactnative.dev/docs/signed-apk-android

keytool -genkeypair -v -storetype PKCS12 -keystore my-upload-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000

ADD ARQUIVO my-upload-key.keystore NA PASTA android/app

ADD NO FIM DO ARQUIVO android/gradle.properties

MYAPP_UPLOAD_STORE_FILE=my-upload-key.keystore

MYAPP_UPLOAD_KEY_ALIAS=my-key-alias

MYAPP_UPLOAD_STORE_PASSWORD=SENHA

MYAPP_UPLOAD_KEY_PASSWORD=SENHA



### NO ARQUIVO android/app/build.gradle
```
signingConfigs {
       ...
       release {
           if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
               storeFile file(MYAPP_UPLOAD_STORE_FILE)
               storePassword MYAPP_UPLOAD_STORE_PASSWORD
               keyAlias MYAPP_UPLOAD_KEY_ALIAS
               keyPassword MYAPP_UPLOAD_KEY_PASSWORD
           }
       }
   }
buildTypes {
      ...
       release {
		...
           signingConfig signingConfigs.release
       }
   }
```




### DEIXAR APK MENOR arquivo android/app/build.gradle
```
def enableProguardInReleaseBuilds = true

react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
```
### ACESSAR PASTA cd android
#### PARA GERAR .AAB ./gradlew bundleRelease
#### PARA GERAR .APK ./gradlew assembleDebug