## Instalação

```
npm i react-content-loader react-native-svg --save
```

## Como usar
index.tsx
```
import ContentLoader, { Circle, Rect } from 'react-content-loader/native';

export default function App() {

    // Altura e largura de forma automatica
    const { height, width } = useWindowDimensions();

    return (
        <ContentLoader 
          viewBox={0 0 ${width} ${height}} 
          backgroundColor='#333' 
          foregroundColor='#999'
        >
          <Circle cx='36' cy='38' r='36'/>
          <Rect x="80" y="17" rx="4" ry="4" width={30} height={12}/>
          <Rect x="80" y="40" rx="4" ry="4" width={200} height={14}/>
        </ContentLoader>
    );
}
```