## Instalação

```
yarn add styled-components@5.3.11
yarn add -D @types/styled-components-react-native
```

## Como usar

src/theme/styled.d.ts
```
import 'styled-components';
import light from './light';

declare module 'styled-components' {
  type ThemeType = typeof light;

  export interface DefaultTheme extends ThemeType { }
}
```


src/theme/light.ts
```
export default {
    COLORS: {
        BACKGROUND: '#820AD1',
        BACKGROUND_CARTAO: '#9500F6',

        TEXT: '#FFFFFF'
    }
}
```


App.tsx
```
import { StatusBar } from 'expo-status-bar';
import { ThemeProvider } from 'styled-components/native';

import light from './src/theme/light';

import Home from './src/screens/Home';

export default function App() {
  return (
    <ThemeProvider theme={light}>
      <StatusBar style='auto' />
      <Home />
    </ThemeProvider>
  );
}

```


styles.ts
```
import styled from "styled-components/native";

import { FlatList,FlatListProps } from 'react-native';
import {Dataprops} from './';

export const Container = styled.SafeAreaView`
    flex: 1;
    background: ${({ theme }) => theme.COLORS.BACKGROUND};
    padding-left: 30px;
    padding-right: 30px;
`;

export const View = styled.View`
    flex: 1;
`;

export const Text = styled.Text`
    padding-top: 30px;
    padding-left: 20px;
    padding-bottom: 30px;
    font-weight: 600;
    font-size: 14px;

    color: ${({ theme }) => theme.COLORS.TEXT};
`;

export const ScrollAjuda = styled.ScrollView`
    /* margin-left: 10px; */
`;

export const ContentScroll = styled.ScrollView.attrs({
    showsVerticalScrollIndicator: false,
})`
    background-color: ${({ theme }) => theme.COLORS.BACKGROUND};
`;

export const Header = styled(LinearGradient).attrs(({ theme })=>({
    colors: theme.COLORS.GRADIENT
}))`
    padding: ${getStatusBarHeight() + 34}px 24px 0;
    
`;

export const List = styled(FlatList as new (props: FlatListProps<string>) => FlatList<Dataprops>)`

`;

```


index.tsx
```
import * as S from './styles';

export type Dataprops = {
    id: string;
    avatar: string;
    name: string;    
    specialist: string; 
}

export default function Home() {
    return (
        <S.Container>
            <S.View>
                <S.Text>
                    Do que precisa?
                </S.Text>
                <S.List
                    data={DATA}
                >
            </S.View>
        </S.Container>
    )
}
```