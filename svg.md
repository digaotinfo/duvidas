## Instalação

```
npx expo install react-native-svg
npm i -D react-native-svg-transformer

```

Criar o arquivo metro.config.js na raiz do projeto
```
const { getDefaultConfig } = require("expo/metro-config");

module.exports = (() => {
  const config = getDefaultConfig(__dirname);

  const { transformer, resolver } = config;

  config.transformer = {
    ...transformer,
    babelTransformerPath: require.resolve("react-native-svg-transformer")
  };
  config.resolver = {
    ...resolver,
    assetExts: resolver.assetExts.filter((ext) => ext !== "svg"),
    sourceExts: [...resolver.sourceExts, "svg"]
  };

  return config;
})();
```

Acrescentar no app.json após o <b>assetBundlePatterns</b>
```
...


"packagerOpts": {
  "config": "metro.config.js",
  "sourceExts": [
    "expo.ts",
    "expo.tsx",
    "expo.js",
    "expo.jsx",
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "wasm",
    "svg"
  ]
},

...
```
