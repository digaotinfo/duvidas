## Funções epecificas

```typescript
// Input 1.55 
// Output: R$ 1,55
export function formatarMoeda(value: number) {
    return value.toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL"
    })
}

```