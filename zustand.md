## Instalação

```bash
npm i zustand
```

Criar pasta /src/stores/store.ts
```typescript
import { create } from 'zustand';
import * as cartInMemory from "./helpers/car-in-memory";

import { createJSONStorage, persist } from 'zustand/middleware';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { ProductProps } from '@/utils/data/products';

export const useCartStore = create(persist<StateProps>((set) => ({
    
    products: [],
    add: (product: ProductProps) => set((state) => ({
        products: cartInMemory.add(state.products, product)
    })),
    remove: ((productId: string) => set((state) => ({
        products: cartInMemory.remove(state.products, productId)
    }))),
    clear:() => set( () => ({ products: [] }) )

}), {
    name: "delivery-digao:cart",
    storage: createJSONStorage(() => AsyncStorage)
}))

```

criar /src/stores/helpers/cart-in-memory.ts
```typescript
import { ProductProps } from "@/utils/data/products";
import { ProductsCartProps } from "../cart-store";

export function add(products: ProductsCartProps[], newProduct: ProductProps) {
    const existingProdutct = products.find(({id}) => newProduct.id === id);

    if (existingProdutct) {
        return products.map((product) => product.id === existingProdutct.id ? {...product, quantity: product.quantity+1} : product)
    }

    return [...products, {...newProduct, quantity: 1}];
}

export function remove(products: ProductsCartProps[], productRemoveId: string) {

    const updatedProducts = products.map((product) => 
        product.id === productRemoveId ? {
            ...product,
            quantity: product.quantity > 1 ? product.quantity-1 : 0
        } : product
    );

    return updatedProducts.filter((product) => product.quantity > 0);
}
```